"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && Symbol.iterator in Object(iter)) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*================================================================================================*/
// Arrow Functions
var greet = function greet(name) {
  console.log("hello ".concat(name));
};

greet('charlie');
/*================================================================================================*/

/*
  What is Rest Parameter?

    when you are not sure of how many parameters the function accepts or if you want to \
    vary the number of parameters while invoking the function then use rest parameters.

    all parameters passed to the function will be condensed into a single array.
*/
// Rest Parameter

var thrice = function thrice() {
  for (var _len = arguments.length, nums = new Array(_len), _key = 0; _key < _len; _key++) {
    nums[_key] = arguments[_key];
  }

  console.log(nums);
  return nums.map(function (num) {
    return num * 3;
  });
};

var resultThrice1 = thrice(1, 2, 3);
var resultThrice2 = thrice(1, 2, 3, 4, 5, 6, 7);
console.log(resultThrice1, resultThrice2);
/*================================================================================================*/
// Spread Operator
// With Arrays

var people = ['Alex', 'Bob', 'Chrlie'];
var coders = ['Dave', 'Edward'].concat(people);
var programmers = [].concat(people, ['Frank', 'Gill']);
console.log(coders, programmers); // With Objects

var person = {
  name: 'Charlie',
  age: 32,
  job: 'coder'
};

var personClone = _objectSpread(_objectSpread({}, person), {}, {
  location: 'A57'
});

console.log(personClone);
/*================================================================================================*/
// sets

var namesArray = ['Alex', 'Bob', 'Alex', 'Charlie'];
console.log(namesArray); // method 1
// const namesSet = new Set(['Alex', 'Bob', 'Alex', 'Charlie']);
// console.log(namesArray);
// method 2

var namesSet = new Set(namesArray);
console.log(namesSet); // const uniqueNames = [...namesSet];
// const uniqueNames = [...new Set(namesArray)];

var uniqueNames = _toConsumableArray(new Set(['Alex', 'Bob', 'Alex', 'Charlie']));

console.log(uniqueNames);
var ages = new Set();
ages.add(20);
ages.add(25).add(30);
ages.add(25);
ages["delete"](30);
console.log(ages, ages.size);
console.log(ages.has(30), ages.has(20));
ages.clear();
console.log(ages);
var codersSet = new Set([{
  name: 'Alex',
  age: 30
}, {
  name: 'Bob',
  age: 29
}, {
  name: 'Charli',
  age: 32
}]);
console.log(codersSet);
/*================================================================================================*/
// symbols

/*
  No two symbols are same.
  usage : when symbols are used as object keys means all keys in that object are always unique.
 */

var symbolOne = Symbol('a generic name');
var symbolTwo = Symbol('a generic name');
console.log(symbolOne, _typeof(symbolOne));
console.log(symbolOne === symbolTwo);
var coderOne = {};
coderOne.age = 30;
coderOne['skills'] = 'android';
coderOne['skills'] = ['js', 'python'];
coderOne['skills'] = [].concat(_toConsumableArray(coderOne.skills), ['kotlin', 'iOS']);
coderOne[symbolOne] = 'programmer';
coderOne[symbolTwo] = 'coder';
console.log(coderOne);
console.log(coderOne[symbolOne], coderOne[symbolTwo]);
/*================================================================================================*/
