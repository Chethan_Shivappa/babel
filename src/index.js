/*================================================================================================*/
// Arrow Functions
const greet = name => {
  console.log(`hello ${name}`);
};

greet('charlie')


/*================================================================================================*/
/*
  What is Rest Parameter?

    when you are not sure of how many parameters the function accepts or if you want to \
    vary the number of parameters while invoking the function then use rest parameters.

    all parameters passed to the function will be condensed into a single array.
*/

// Rest Parameter
const thrice = (...nums) => {
  console.log(nums)
  return nums.map(num => num * 3)
}

const resultThrice1 = thrice(1,2,3);

const resultThrice2 = thrice(1,2,3,4,5,6,7);

console.log(resultThrice1, resultThrice2)

/*================================================================================================*/

// Spread Operator

// With Arrays
const people = ['Alex', 'Bob', 'Chrlie']
const coders = ['Dave', 'Edward', ...people]
const programmers = [...people, 'Frank', 'Gill']

console.log(coders, programmers)

// With Objects

const person = { name: 'Charlie', age: 32, job: 'coder'}
const personClone = { ...person, location: 'A57'}

console.log(personClone)




/*================================================================================================*/

// Sets
const namesArray = ['Alex', 'Bob', 'Alex', 'Charlie'];
console.log(namesArray);

// method 1
// const namesSet = new Set(['Alex', 'Bob', 'Alex', 'Charlie']);
// console.log(namesArray);


// method 2
const namesSet = new Set(namesArray);
console.log(namesSet);

// const uniqueNames = [...namesSet];
// const uniqueNames = [...new Set(namesArray)];
const uniqueNames = [...new Set(['Alex', 'Bob', 'Alex', 'Charlie'])];
console.log(uniqueNames);




// Methods that can be called on sets.
const ages = new Set();
ages.add(20);
ages.add(25).add(30);
ages.add(25);
ages.delete(30)

console.log(ages, ages.size);
console.log(ages.has(30), ages.has(20));

ages.clear();
console.log(ages);

const codersSet = new Set([
  {name: 'Alex', age: 30},
  {name: 'Bob', age: 29},
  {name: 'Charli', age: 32}
]);

console.log(codersSet)

/*================================================================================================*/

// symbols

/*
  No two symbols are same.
  usage : when symbols are used as object keys means all keys in that object are always unique.
 */

const symbolOne = Symbol('a generic name');
const symbolTwo = Symbol('a generic name');

console.log(symbolOne, typeof symbolOne);
console.log(symbolOne === symbolTwo);

const coderOne = {};

coderOne.age = 30;
coderOne['skills'] = 'android';
coderOne['skills'] = ['js', 'python'];
coderOne['skills'] = [...coderOne.skills ,'kotlin', 'iOS'];

coderOne[symbolOne] = 'programmer';
coderOne[symbolTwo] = 'coder';

console.log(coderOne);
console.log(coderOne[symbolOne], coderOne[symbolTwo]);

/*================================================================================================*/


